import sqlite3

class AnalyticsWorkItem:
    def __init__(self, db_file_name):
        self.db_file_name = db_file_name

    def connect(self):
        con = sqlite3.connect(self.db_file_name, timeout=10)
        return con

    def getNextWorkItem(self, previous=0):
        con = self.connect()
        cur = con.cursor()
        res = cur.execute("SELECT w.id, w.total_step, w.current_step, ga.ga_id, tw.name, tw.date_from, tw.date_to, tw.period, p.name, p.dimensions, p.metrics, p.sort_by, p.max_results \
                  FROM work w \
                  JOIN ga_property ga ON ga.id = w.ga_property_id \
                  JOIN time_window tw ON tw.id=w.time_window_id \
                  JOIN projection p ON p.id=w.projection_id \
                  WHERE w.current_step < w.total_step \
                  ORDER BY w.current_step ASC, w.id")
        [work_id, steps, current, ga_id, time_window_name, date_from, date_to, period, projection_name, dimension, metrics, sort_by, max_res] = res.fetchone()
        cur.close()
        if previous >= steps:
            # reached the end of the work, no next item
            # update the current step
            self.storeCurrentStep( work_id, steps)
            return None 

        return [work_id, steps, current, ga_id, time_window_name, date_from, date_to, period, projection_name, dimension, metrics, sort_by, max_res]

    def markStepAsDone(self, work_id):
        con = self.connect()
        cur = con.cursor()
        cur.execute("UPDATE work SET current_step = current_step+1 WHERE id = ?", [work_id])
        cur.close()
        con.commit()
        con.close()
        

    def storeCurrentStep(self, work_id, step):
        con = self.connect()
        cur = con.cursor()
        cur.execute("UPDATE work SET current_step = ? WHERE id = ?", [step, work_id])
        cur.close()
        con.commit()
        con.close()
        
        
