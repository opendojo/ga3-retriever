from googleapiclient import discovery
from googleapiclient.http import build_http
from oauth2client import client, file, tools
import pydata_google_auth
from oauth2client.service_account import ServiceAccountCredentials

import httplib2

class Ga3Retriever:
    SCOPES = ['https://www.googleapis.com/auth/analytics.readonly']
    
    def __init__(self, json_file_path):
        self.json_file_path = json_file_path
        self.credential = self.createCredential(self.json_file_path)
        self.service = self.createService(self.credential)
    
    def createCredential(self, json_file_path):
        credential = ServiceAccountCredentials.from_json_keyfile_name(json_file_path, self.SCOPES)
        if not credential or credential.invalid:
            print('Unable to authenticate using service account key.', file=sys.stderr)
        return credential

    def createService(self, credential):
        http_auth = credential.authorize(httplib2.Http())
        service = discovery.build('analytics', 'v3', http=http_auth)
        return service

    def getData(self, ga_id, start_date, end_date, dimensions, metrics, sort, max_results):
        return self.service.data().ga().get(
            ids=ga_id,
            start_date=start_date,
            end_date=end_date,
            dimensions=dimensions,
            sort=sort,
            max_results=max_results,
            metrics=metrics).execute()