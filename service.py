# Service that runs and retrieve the DATA from a pre-loaded SQlite Database

from ga3retriever import Ga3Retriever
from analyticsworkitem import AnalyticsWorkItem
import json
import os
import sqlite3
import datetime
import time
import random
from googleapiclient import errors
from httplib2 import HttpLib2Error
from slack_sdk import WebClient
import tomllib

with open("settings.toml", "rb") as f:
    settings = tomllib.load(f)

print(settings)

analytics = Ga3Retriever(settings['CLIENT_SECRETS_PATH'])
work = AnalyticsWorkItem(settings['DB_FILE_NAME'])

current_step = 0
retry = 0

def SendSlackMessage(message, slack_token=settings['SLACK_TOKEN'], channel=settings['SLACK_CHANNEL'], username=settings['SLACK_USERNAME']):
    if slack_token != None and len(slack_token) > 0:
        # Set up a WebClient with the Slack OAuth token
        client = WebClient(token=slack_token)
        
        # Send a message
        client.chat_postMessage(
            channel=channel, 
            text=message, 
            username=username
        )    

SendSlackMessage("Starting the service")

while True:
    if retry > settings['MAX_RETRIES']:
        print("MAX_RETRIES "+str(settings['MAX_RETRIES'])+" reached")
        SendSlackMessage("Giving up max retries reached! Need human intervention.")
        break;
    try:
        [work_id, steps, current, ga_id, time_window_name, date_from, date_to, period, projection_name, dimension, metrics, sort_by, max_res] = work.getNextWorkItem(current_step)
    except TypeError:
        print("Everything done!")
        SendSlackMessage("That's it we are done! all works item have been processed.")
        break;
    [year,month,day] = date_from.split('-')
    
    datetime_from = datetime.datetime.fromisoformat(date_from) + datetime.timedelta(period*current)
    datetime_to = datetime.datetime.fromisoformat(date_from) + datetime.timedelta(period*current+period)

    folder = settings['DATA_DIR']+datetime_from.strftime("%Y")+'/'+datetime_from.strftime("%m")+'/'

    message = "Working on "+projection_name+" ("+str(current)+"/"+str(steps)+")"
    print(message)
    SendSlackMessage(message)

    try:
        data = analytics.getData(ga_id, datetime_from.strftime("%Y-%m-%d"), datetime_to.strftime("%Y-%m-%d"), dimension, metrics, sort_by, max_res)
        data_json = json.dumps(data, indent=4)
        
        if not os.path.exists(folder):
           os.makedirs(folder)
        
        file_path = folder+projection_name+"_"+time_window_name+"_"+datetime_from.strftime("%d")+".json"
        
        with open(file_path, "w") as outfile:
            outfile.write(data_json)
        
        work.markStepAsDone(work_id)
        retry = 0
        time.sleep(random.randint(settings['FAIR_MIN_DELAY'], settings['FAIR_MAX_DELAY']))
    except (errors.HttpError, HttpLib2Error) as e:
        print("Got error HttpError")
        retry = retry + 1
        message = "Retrying... ("+str(retry)+"/"+str(settings['MAX_RETRIES'])+")" 
        print(message)
        SendSlackMessage(message)
        time.sleep(random.randint(settings['FAIR_MIN_RETRY_DELAY'],settings['FAIR_MAX_RETRY_DELAY']))

