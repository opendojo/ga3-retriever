# Ga3 Retriever

The 1st of July 2024 Google Analytics 3 - Universal Analytics 3 (UA3) - will be definetively removed. No more access to the interface, nor the data.
If you want to keep some of the key values of your website. Here is a small bunch of script to do so.

## Disclaimer

This repository is a bunch of quick and dirty script I setup in order to retrieve the data. Don't excpect high quality code.
However all the code has been made to be relayable, to support network issue and API errors.
I hope that if you use this bunch of tools you will be able to retrieve the data for the remaining days the UA3 exists.

I choose python because of the python notebook that is quite efficient to test and learn quickly.

Also keep in mind that I never learned so fast a library that will be obsolete in a such short time. So no need to have extensive focus or quality in the writing.

## Getting started

Everything is based on a debian compatible system.

So it could be a docker, a virtual machine, a real server ... up to you.

### Install

```bash
mkdir -p ~/workspace/
git clone git@gitlab.com:opendojo/ga3-retriever.git
cd ga3-retriever
python3 -m venv venv
source venv/bin/activate
pip3 install -r requirement.txt
```

### Run Jupyter lab

```bash
jupyter lab
```

### Google Analytics property

Every GA property has an internal ID which you need to find in order to make all the API call.

You will also need a service IAM user.

#### Creating a Service IAM user

See: https://developers.google.com/analytics/devguides/config/mgmt/v3/quickstart/service-py Step 1

Don't forget to download the keys in JSON format. Name this file google-service-secret.json and you are set.

#### Find te ga:id of the Google analytics property

In the [Account Explorer](https://ga-dev-tools.google/account-explorer/) once logged in, you will have access to the Table ID of the property you are looking for.
this table id must be set in the [Create the database] section.

Otherwise you can use the [find_ga.ipynb](http://localhost:8888/lab/tree/find_ga.ipynb)

### Create the database

In [your local Jupyter Lab](http://localhost:8888/lab/tree/create_database.ipynb) 

The file create database help you create a SQLite database that will be used to create the work item for the service script.

This allow the service script to keep track of advancement as the process may need 1 day per year of data depending of the information you want to retrieve.

There are already 5 type of dashboard which are called **projection**:
* behavior_page_views - list of page views (top 500) with number of views
* aquisition_source_medium - list of source/medium for aquisition traffic
* technology_os - list of operating system
* technology_screen - list of screen size
* behavior_landing_path - list of landing page and second page

For every type of dashboard there specific dimensions and metrics. Be sure to test them.

For every projection there is a limit, theoritically 5000 (or maybe more) but ost of the time, higher than 500+ raise a lot of api errors (timeout on google side).

A great ressource to define dimensions and metrics: https://ga-dev-tools.google/dimensions-metrics-explorer/

Then there are the time windows. Depending on how you want to agregate the data, you can request for 1 day, for a week, for a month, for any period you want (quarters?). Everything will be agregated. Also it's important to know that large period of time raise a lot of errors on API side.

So I used 1 day and I setup a from and to, so the system will generate all the request to get all the 1 day period between 2 dates (from, to).

Then you have your ga:xxxx property. Put your own property in the create_database notebook.

## Collaborate 

Fill free to fork ! If you want make an issue and a pull request. But don't expect amazing support. By the time you will be reading this, it will probably be obsolete. 
